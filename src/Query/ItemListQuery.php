<?php

namespace tbradbury\DynamicsNav\Query;

use tbradbury\DynamicsNav\DynamicsNavClient;
use tbradbury\DynamicsNav\Entity\Item;

/**
 * Class ItemLiseQuery.
 */
class ItemListQuery
{

    /**
     * The client to use to communicate with Dynamics Nav.
     *
     * @var \tbradbury\DynamicsNav\DynamicsNavClient
     */
    protected $client;

    /**
     * The response from the API.
     *
     * @var string
     */
    protected $response;

    /**
     * The URL to query for more items.
     *
     * @var string
     */
    protected $nextUrl;

    /**
     * ItemQuery constructor.
     *
     * @param \tbradbury\DynamicsNav\DynamicsNavClient $client
     *   A client to connect to Dynamcis Nav with.
     */
    public function __construct(DynamicsNavClient $client)
    {
        $this->client = $client;
    }

    /**
     * Get a list of products.
     *
     * @return mixed|false
     *   The response from the API or FALSE if there is no more data.
     */
    public function fetch()
    {
        $url = $this->nextUrl();
        if (!$url) {
            return false;
        }
        $this->response = json_decode($this->client->request($url));
        $this->nextUrl = !empty($this->response->{'@odata.nextLink'}) ? $this->response->{'@odata.nextLink'} : false;
        return array_map(function ($item) {
            return new Item($item);
        }, $this->response->value);
    }

    /**
     * The next URL to request to fetch data.
     *
     * @return string|false
     *   A URL or FALSE if there is no more data.
     */
    public function nextUrl()
    {
        if ($this->nextUrl === false) {
            return false;
        }
        return ($this->nextUrl) ? $this->nextUrl : $this->client->url('ItemList');
    }
}
