<?php

namespace tbradbury\DynamicsNav\Query;

use tbradbury\DynamicsNav\DynamicsNavClient;
use tbradbury\DynamicsNav\Entity\Item;

/**
 * Class ItemQuery.
 */
class ItemQuery
{

    /**
     * The client to use to communicate with Dynamics Nav.
     *
     * @var \tbradbury\DynamicsNav\DynamicsNavClient
     */
    protected $client;

    /**
     * The item number to query.
     *
     * @var string
     */
    protected $number;

    /**
     * The response from the API.
     *
     * @var string
     */
    protected $response;

    /**
     * The URL to query for more items.
     *
     * @var string
     */
    protected $nextUrl;

    /**
     * ItemQuery constructor.
     *
     * @param \tbradbury\DynamicsNav\DynamicsNavClient $client
     *   A client to connect to Dynamcis Nav with.
     * @param string $number
     *   An item number.
     */
    public function __construct(DynamicsNavClient $client, $number)
    {
        $this->client = $client;
        $this->number = $number;
    }

    /**
     * Get a new instance with a different number.
     *
     * @param string $number
     *   An item number.
     *
     * @return static
     */
    public function withNumber($number)
    {
        return new static($this->client, $number);
    }

    /**
     * Get a single product by its item number.
     *
     * @return \tbradbury\DynamicsNav\Entity\Item
     *   An object to represent the item.
     */
    public function fetch()
    {
        return new Item(json_decode($this->client->request($this->client->url("/Item('{$this->number}')"))));
    }
}
