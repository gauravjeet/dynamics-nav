<?php

namespace tbradbury\DynamicsNav;

/**
 * Interface DynamicsNavClientInterface.
 */
interface DynamicsNavClientInterface
{

    /**
     * Create a new Dynamics Nav client.
     *
     * @param string $base_url
     *   The base URL for all endpoints.
     * @param string $username
     *   The username for basic authentication.
     * @param string $password
     *   The password for basic authentication.
     * @param string $company
     *   The company to use.
     */
    public function __construct($base_url, $username, $password, $company);

    /**
     * Get a new instance with a different company.
     *
     * @param string $company
     *   The new company to use.
     *
     * @return static
     */
    public function withCompany($company);

    /**
     * Send a request.
     *
     * Uses POST if $data is provided. Uses GET otherwise.
     *
     * @param string $url
     *   The URL to request.
     * @param mixed $data
     *   Data to POST to the API.
     *
     * @return mixed
     *   The response from the API.
     */
    public function request($url, $data = null);
}
