<?php

namespace tbradbury\DynamicsNav\Entity;

/**
 * Class Entity.
 */
class Entity
{

    protected $data;

    /**
     * Entity constructor.
     *
     * @param object $data
     *   The entity's data.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get a value from the entity's data.
     *
     * @param string $property
     *   A property to get from the entity.
     *
     * @return mixed
     *   The value of the property or NULL.
     */
    public function __get($property = null)
    {
        if (!empty($property)) {
            if (isset($this->data->$property)) {
                return $this->data->$property;
            } else {
                return null;
            }
        } else {
            return $this->data;
        }
    }

    /**
     * Get a JSON representation of the entity.
     *
     * @return string
     *   A JSON representation of the entity.
     */
    public function asJson()
    {
        return json_encode($this->asArray());
    }

    /**
     * Get the entity as an associative array.
     *
     * @return array
     *   The entity represented by an associative array.
     */
    public function asArray()
    {
        $entity = [];
        foreach ($this->data as $key => $value) {
            $entity[$key] = $value;
        }
        return $entity;
    }
}
