<?php
declare(strict_types=1);

namespace tbradbury\DynamicsNav\tests;

use PHPUnit\Framework\TestCase;
use tbradbury\DynamicsNav\Entity\SalesOrder;

/**
 * @covers SalesOrder
 */
final class SalesOrderTest extends TestCase
{

    public function testGetSalesOrder()
    {
        $client = new MockDynamicsNavClient('', '', '', '');
        $sales_order = SalesOrder::fromQuery($client, 'ABC123');
        $this->assertSame("ABC123", $sales_order->No);
    }
}
