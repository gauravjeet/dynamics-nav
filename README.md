# *Woo!*

# TODO
- Take a PSR7 request in the client instead of curl opts in the method?
- Change namespace.
- Add PHPUnit / mock test.
- Add (GitLab) CI to run style, phpunit, etc.
- Add CodeClimate.
  - Configure phpmd, etc.

# Usage

```php
$client = new DynamicsNavClient($base_url, $username, $password, $company);

$item_list = new ItemListQuery($client);
/** @var Item[] $items */
$items = [];
while ($result = $item_list->fetch()) {
  $results = array_merge($results, $result);
}

/** @var Item $item */
$item = Item::fromQuery($client, '001-300');

$sales_order = (object) [
  'DocumentType' => 'Order',
  'Sell_to_Customer_Name' => 'Travis',
];
$sales_order = new SalesOrder($sales_order)->save();
// $sales_order->No => "12345".
```
